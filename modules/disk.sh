#!/bin/bash

## disk
echo "{ \"disk\" : ["

    /bin/df -Ph \
        | /bin/awk 'NR>1 {print "{\n \"file-system\" : \"" $1 "\",\n \"capacity-kb\" : \"" $2 "\",\n \"mount\" : \"" $6 "\"\n },"}END{print "{\n \"file-system\" : \"" $1 "\",\n \"capacity\" : \"" $2 "\",\n \"mount\" : \"" $6 "\"\n }"}'

echo "] }";

