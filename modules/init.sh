#!/bin/bash
/bin/echo "{"

## CPU Core
/usr/bin/lscpu \
    | /bin/grep "^CPU(s):" \
    | /bin/awk '{print " \"cpu-core\" : " $2 ","}'
#   | /bin/awk '{match($0,/CPU\(s\):([\s|/t])*(.d)+/,a)}END{print " \"cpu-cores\" : " a[0] ", "}'

## CPU Model
/usr/bin/lscpu \
    | /bin/grep 'Model name:' \
    | /bin/awk '{match($0,/Model\s?name:(\s)*(.*)/,a)}END{print " \"cpu-model\" : " "\"" a[2] "\","}'

## Memory
/bin/cat /proc/meminfo \
    | /bin/awk 'NR == 1 {print " \"memory-total-kB\" : " $2 }' \

/bin/echo "}"
