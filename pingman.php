<?php
/*
    Plugin Name: Pingman Client
    Author: R-Harada
    Version: 1.0
 */

class Pingman
{

    private $format;

    public function __construct()
    {
        register_activation_hook(__FILE__, array($this, 'RouterAddFlush'));
        add_action('init', array($this, 'RouterRule'));
        add_filter('query_vars', array( $this, 'RouterQuery' ));
        add_action('template_redirect', array($this, 'controller'));
        add_action('pingmanInstall', array($this, 'pingmanInstall'));
        add_filter('redirect_canonical', array($this, 'mytheme_redirect_canonical'), 10, 2 );

    }

    /**
     *  リライトルール追加
     *
     *  @author R-Harada
     *  @return void
     */
    public function RouterRule()
    {
        add_rewrite_rule('^pingman/(.*)?', 'index.php?ps_pingman=$matches[1]', 'top');
    }

    /**
     *  リライトクエリ追加
     *      リライトルールを追加後必須
     *
     *  @author R-Harada
     *  @param  $query_vars Array
     *  @return $query_vars Array
     */
    public function RouterQuery($query_vars = array())
    {
        $query_vars[] = 'ps_pingman';
        return $query_vars;
    }

    /**
     *  リライト情報保存
     *
     *  @author R-Harada
     *  @return void
     */
    public function RouterAddFlush()
    {
        $this->RouterRule();
        flush_rewrite_rules(false);
    }

    /**
     *  コントローラー
     *
     *  @author R-Harada
     *  @return void
     */
    public function controller()
    {

        global $wp_query;
        $routerQuery = isset($wp_query->query_vars['ps_pingman']) ? $wp_query->query_vars['ps_pingman'] : '';
        $routerAction = preg_split('/\./', $routerQuery);
        $this->format = empty($routerAction[1]) ? 'json' : $routerAction[1];

        if($routerQuery) {
            switch($routerAction[0]) {
                case 'install':
                    do_action('pingmanInstall');
                    break;
                case 'all':
                    
                    break;
                case 'interval':

                    break;
                default:
                    $wp_query->set_404();
            }
        }

    }

    /**
     *  画面出力
     *
     *  @author R-Harada
     *  @param  Array   @content    内容
     *  @return void
     */
    public function sendResponce($content = array())
    {
        add_filter( 'template_include', function() { return false; });
        $this->getHeader();
        echo $this->formatContent($content);
    }

    /**;
     *  タイプ別にHeaderを指定する
     *
     *  @author R-Harada
     *  @return void
     */
    private function getHeader()
    {
        switch($this->format) {
            case 'json':
                $header = 'application/json;';
                break;
            case 'inc':
                $header = 'text/plain;';
                break;
#            case 'xml':
#                $header = 'text/xml;';
#                break;
            default:
                $header = 'application/json;';
        }

        header('Content-Type: ' . $header,  'charset=UTF-8;');

    }

    private function formatContent($content = array())
    {

        switch($this->format) {
            case 'json':
                $content = json_encode($content);
                break;
            case 'inc':
                $content = serialize($content);
                break;
#            case 'xml':
#                $obj = array_flip($content);
#                $xml = new SimpleXMLElement('<pingman />');
#                array_walk_recursive($obj, array($xml, 'addChild'));
#                $content = $xml->asXML();
#                break;
            default:
                $content = json_encode($content);
        }

        return $content;

    }

    /**
     *  インストール用アクション
     *
     *  @author R-Harada
     *  @return void
     */
    public function pingmanInstall()
    {

        global $wp_query;
        $status = get_option('pingman_token', false);
        if(!$status) {

            if($this->tokenValidate()) {
                update_option('pingman_token', $_POST['pm_token']);
                $initData = json_decode(shell_exec("/bin/sh " . __DIR__ . "/modules/init.sh"), true);
                $this->sendResponce($initData);
            } else {
                $wp_query->set_404();
            }

        } else {
            $wp_query->set_404();
        }

    }

    /**
     *  pm_token 存在確認とバリデート
     *
     *  @author R-Harada
     *  @return boolean
     */
    private function tokenValidate()
    {
        $token = $_POST['pm_token'];
        $status = empty($token) ? false : true;
        if($status) {
            $status = preg_match('/^[a-zA-Z0-9]{20}$/' ,$token);
        }
        return $status;

    }

    public function mytheme_redirect_canonical( $redirect_url, $requested_url ) {
            return $requested_url;
    }

}

new Pingman();
